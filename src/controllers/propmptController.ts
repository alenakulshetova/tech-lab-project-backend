import { Request, Response } from "express";

import Prompt from "../entities/prompt";

export let allPrompts = (req: Request, res: Response) => {
  let prompts = Prompt.find((err, prompts) => {
    if (err) {
      res.send("Error!");
    } else {
      res.send(prompts);
    }
  });
};

export let getPrompt = (req: Request, res: Response) => {
  let prompt = Prompt.findById(req.params.id, (err: any, prompt: any) => {
    if (err) {
      res.send(err);
    } else {
      res.json(prompt.id);
    }
  });
};
