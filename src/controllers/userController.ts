import { Request, Response } from "express";
import { hash, compare } from "bcrypt";
import jwt from "jsonwebtoken";
import User, { IUser } from "../entities/user";
import { JWTSecret } from "../app";

export let getUser = async (req: Request, res: Response) => {
  const parsedToken = req.params.token;
  await jwt.verify(parsedToken, JWTSecret, function (err, decoded) {
    User.findById(decoded.id, (err: any, user: any) => {
      if (err) {
        res.send(err);
      } else {
        res.send(user);
      }
    });
  });
};

export let registerUser = async (req: Request, res: Response) => {
  if (req.body == undefined) {
    return res.status(404).send("Body is undefined");
  }

  var user = new User(req.body);

  user.save(async (err, newUser) => {
    if (err) {
      res.send(err);
    } else {
      res.send(await getAuthToken(newUser));
    }
  });
};

export let authUser = async (req: Request, res: Response) => {
  if (req.body == undefined) {
    return res.status(404).send("Body is undefined");
  }

  const params = req.body as { email: string; password: string };

  let user = await User.findOne(
    {
      email: params.email,
    },
    async (err: any, user: any) => {
      if (err) {
        res.send(err);
      }
    }
  );

  if (user != null && (await compare(params.password, user.passwordHash))) {
    return res.json({
      token: await getAuthToken(user),
    });
  }

  return res.send("error");
};

// createss token for 24h
function getAuthToken(user: IUser): Promise<string> {
  return new Promise((resolve, reject) =>
    jwt.sign({ id: user.id }, JWTSecret, { expiresIn: 3600 }, (err, token) => {
      if (err) return reject(err);
      else return resolve(token);
    })
  );
}
