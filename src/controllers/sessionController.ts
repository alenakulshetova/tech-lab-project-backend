import { Request, Response } from "express";

import Session from "../entities/session";

export let allSessions = (req: Request, res: Response) => {
  let sessions = Session.find((err, sessions) => {
    if (err) {
      res.send("Error!");
    } else {
      res.send(sessions);
    }
  });
};

export let getSession = (req: Request, res: Response) => {
  let session = Session.findById(req.params.id, (err, session) => {
    if (err) {
      res.send(err);
    } else {
      res.send(session);
    }
  });
};

export let deleteSession = (req: Request, res: Response) => {
  let session = Session.deleteOne({ _id: req.params.id }, (err) => {
    if (err) {
      res.send(err);
    } else {
      res.send("Successfully Deleted session");
    }
  });
};

export let updateSession = (req: Request, res: Response) => {
  console.log(req.body);
  //   Session.findOneAndUpdate({ _id: req.params.sessionID }, req.body, { new: true }, (err, contact) => {
  //     if(err){
  //         res.send(err);
  //     }
  //     res.json(session);
  // });

  let session = Session.findByIdAndUpdate(
    req.params.id,
    req.body,
    (err, session) => {
      if (err) {
        res.send(err);
      } else {
        res.send("Session.findByIdAndUpdate success");
      }
    }
  );
};

export let addSession = (req: Request, res: Response) => {
  var session = new Session({
    propmtID: req.params.propmtID,
    duration: req.params.duration,
    text: req.body,
  });

  session.save((err) => {
    if (err) {
      res.send(err);
    } else {
      // res.json(session); ???
      res.send(session);
    }
  });
};
