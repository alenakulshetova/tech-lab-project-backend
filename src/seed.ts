import { hash } from "bcrypt";

export const PROD_MONGODB_URI = `mongodb+srv://alena:AlPas.123@cluster0-czgdj.mongodb.net/test?retryWrites=true&w=majority`;

import prompts from "../data/prompts.json";
import Prompt from "./entities/prompt";
import User from "./entities/user";

async function seed() {
  // reset db data
  await Prompt.deleteMany({});
  await User.deleteMany({});

  console.log("Cleared Database");

  // users
  const user = new User({
    fullName: "Alena Kulshetova",
    email: "kulshetova@seznam.cz",
    passwordHash: await hash("demo", 12),
  });
  await user.save();
  console.log("User inserted");

  // prompts
  const promptsResult = new Array<Promise<any>>();

  for (const prompt of prompts as Array<string>) {
    promptsResult.push(
      new Prompt({
        title: prompt,
      }).save()
    );
  }
  await Promise.all(promptsResult);
  console.log("Prompts inserted");
}

seed();
