import mongoose from "mongoose";
import { PROD_MONGODB_URI } from "../app";

mongoose.connect(PROD_MONGODB_URI, { useNewUrlParser: true }, (err: any) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Successfully Connected user.ts");
  }
});

export interface IUser extends mongoose.Document {
  fullName: string;
  email: string;
  passwordHash: string;
}

export const UserSchema = new mongoose.Schema({
  fullName: { type: String, required: true },
  email: { type: String, required: true },
  passwordHash: { type: String, required: true },
});

const User = mongoose.model<IUser>("User", UserSchema);

export default User;
