import mongoose from "mongoose";
import { PROD_MONGODB_URI } from "../app";

mongoose.connect(PROD_MONGODB_URI, { useNewUrlParser: true }, (err: any) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Successfully Connected session.ts!");
  }
});

export interface ISession extends mongoose.Document {
  promptID: string;
  duration: number;
  text: string;
}

export const SessionSchema = new mongoose.Schema({
  promptID: { type: String, required: true },
  duration: { type: Number },
  text: { type: String },
});

const Session = mongoose.model<ISession>("Book", SessionSchema);

export default Session;
