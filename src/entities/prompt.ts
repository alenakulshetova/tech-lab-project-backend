import mongoose from "mongoose";
import { PROD_MONGODB_URI } from "../app";

mongoose.connect(PROD_MONGODB_URI, { useNewUrlParser: true }, (err: any) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Successfully Connected prompt.ts");
  }
});

export interface IPrompt extends mongoose.Document {
  title: string;
}

export const PromptSchema = new mongoose.Schema({
  title: { type: String, required: true },
});

const Pompt = mongoose.model<IPrompt>("Prompt", PromptSchema);

export default Pompt;
