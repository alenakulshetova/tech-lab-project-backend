import express, { Request, Response } from "express";
import cors from "cors";
import bodyParser from "body-parser";

export const PROD_MONGODB_URI = `mongodb+srv://alena:AlPas.123@cluster0-czgdj.mongodb.net/test?retryWrites=true&w=majority`;
export const JWTSecret = "myTotalySecretKey";

import * as userController from "./controllers/userController";
import * as promptController from "./controllers/propmptController";
import * as sessionController from "./controllers/sessionController";

// Our Express APP config
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(cors());

app.set("port", process.env.PORT || 3000);

// API Endpoints
app.get("/", (req: Request, res: Response) => res.send("API is running!"));

// API Endpoints
// user
app.get("/user/:token", userController.getUser);
app.post("/user-register", userController.registerUser);
app.post("/user-auth", userController.authUser);

// prompt
app.get("/prompt/:id", promptController.getPrompt);
app.get("/prompt-list", promptController.allPrompts);

// session
app.get("/session/:id", sessionController.getSession);
app.get("/session-list", sessionController.allSessions);
app.post("/session-add/:promptID/:duration", sessionController.addSession);

const server = app.listen(app.get("port"), () => {
  console.log("App is running on http://localhost:%d", app.get("port"));
});
